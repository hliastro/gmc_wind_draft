import glob
import matplotlib.pyplot as plt
from loadmodules import *
import sys

plt.switch_backend('agg')

path = sys.argv[1]

files = glob.glob(path+'/snap_*')
GAMMA = 5/3.
m_p = 1.67e-24
kb = 1.38e-16

for f in np.sort(files):
    i = int(f[-8:-5])
    print i
    s = gadget_readsnap(i, snappath=path, snapbase='snap_')

    pos = s.data['pos']
    vel = s.data['vel']
    mass = s.data['mass']
    pres = s.data['pres']
    rho = s.data['rho']
    type = s.data['type']

    s.data['temp'] = (GAMMA-1.0)*s.data['u']*1e10*m_p/kb
    #s.plot_Aslice('u',logplot=False, colorbar=True, cblabel=True, numthreads=8)
    #s.plot_Aslice('rho',logplot=True,axes=[1,2], vrange=[1e-8,5e-3], numthreads=8)
    #s.plot_Aslice('rho',logplot=False,axes=[1,2], vrange=[1e-8,1.5e-5], numthreads=8)
    s.plot_Aslice('temp',logplot=True,axes=[0,1], box=[30,30,30], vrange=[1,100], numthreads=8)
    cb = plt.colorbar()
    plt.savefig("T_1e5_cooling"+str(i))
    plt.clf()
