import glob
import matplotlib.pyplot as plt
from loadmodules import *
import sys
plt.switch_backend('agg')
from analysis_tools import *

# read Kroupa wind data
kroupaWind = np.loadtxt('Kroupa_wind_FIRE.txt')
from scipy import interpolate
f_dm = interpolate.interp1d(kroupaWind[:,0], kroupaWind[:,1]) # fraction of mass loss
f_e = interpolate.interp1d(kroupaWind[:,0], kroupaWind[:,2])  # wind energy per mass in unit of erg/Msun

path = sys.argv[1]
outputName = sys.argv[2]
files = glob.glob(path+'/snap_*')

GAMMA = 5/3.
mSun = 1.998e33
mP = 1.67e-24
kb = 1.38e-16
pc = 3.086e18
yr = 3.15e7
UnitTime = pc/1e5/yr/1e6    # code time in Myr

mGMC = 5e4
rGMC = 15

dataOut = []

for f in np.sort(files):
    i = int(f[-8:-5])
    print i
    s = gadget_readsnap(i, snappath=path, snapbase='snap_')
    t = s.getTime() * UnitTime

    # half-mass radius
    if (len(s.data_type4['mass'])>10):
        rStarLevel = mass_contour(s.data_type4['pos'], s.data_type4['mass'])
    else:
        rStarLevel = np.zeros(49)

    dataOut.append(np.insert(rStarLevel, 0, t))

dataOut = np.array(dataOut)
np.savetxt(outputName+'.txt',dataOut, fmt="%g")
