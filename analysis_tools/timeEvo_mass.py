import glob
import matplotlib.pyplot as plt
from loadmodules import *
import sys
plt.switch_backend('agg')
plt.style.use('niceplot2')

GAMMA = 5/3.
mSun = 1.998e33
mP = 1.67e-24
kb = 1.38e-16
pc = 3.086e18
yr = 3.15e7
UnitTime = pc/1e5/yr/1e6    # code time in Myr

# read Kroupa wind data
kroupaWind = np.loadtxt('Kroupa_wind_FIRE.txt')
from scipy import interpolate
f_dm = interpolate.interp1d(kroupaWind[:,0], kroupaWind[:,1]) # fraction of mass loss
f_e = interpolate.interp1d(kroupaWind[:,0], kroupaWind[:,2])  # wind energy per mass in unit of erg/Msun

path = sys.argv[1]
files = glob.glob(path+'/snap_*')

dataOut = []

for f in np.sort(files):
    i = int(f[-8:-5])
    s = gadget_readsnap(i, snappath=path, snapbase='snap_')
    t = s.getTime() * UnitTime

    pos = s.data['pos']
    vel = s.data['vel']
    mass = s.data['mass']
    pres = s.data['pres']
    utherm = s.data['u']
    rho = s.data['rho']
    pot = s.data['pot']
    type = s.data['type']
    totBox = np.sum(mass)
    totGas = np.sum(s.data_type0['mass'])

    boundGas = s.data_type0['pot']+np.sum(s.data_type0['vel']**2,axis=1)<0
    mBoundGas = np.sum(s.data_type0['mass'][boundGas])
    mUnboundGas = totGas - mBoundGas

    if len(s.data_type4['mass'])>0:
        massStar = s.data_type4['mass']
        imass = s.data['gima']
        ageStar = s.data_type4['age'] * UnitTime # in Myr
        totStar = np.sum(massStar)
        totStari = np.sum(imass)
    else:
        totStar = 0.0
        totStari = 0.0

    print t, totBox, totGas, mBoundGas, mUnboundGas, totStar, totStari
    dataOut.append([t, totBox, totGas, mBoundGas, mUnboundGas, totStar, totStari])

dataOut = np.array(dataOut)
np.savetxt('mass_evolve.txt', dataOut)

plt.plot(dataOut[:,0], dataOut[:,1], label='Tot')
plt.plot(dataOut[:,0], dataOut[:,2], label='Gas')
plt.plot(dataOut[:,0], dataOut[:,3], label='Bound Gas')
plt.plot(dataOut[:,0], dataOut[:,4], label='Unbound Gas')
plt.plot(dataOut[:,0], dataOut[:,5], label='Star')
plt.plot(dataOut[:,0], dataOut[:,6], label='StarInit')
plt.legend()
plt.savefig("mass_evolve.pdf")
