import glob
import matplotlib.pyplot as plt
from loadmodules import *
import sys

plt.switch_backend('agg')

GAMMA = 5/3.
mSun = 1.998e33
mP = 1.67e-24
kb = 1.38e-16
pc = 3.086e18
yr = 3.15e7
UnitTime = pc/1e5/yr/1e6    # code time in Myr

path = sys.argv[1]

outPref = sys.argv[2]

files = glob.glob(path+'/snap_*.hdf5')

for f in np.sort(files):
    i = int(f[-8:-5])
    s = gadget_readsnap(i, snappath=path, snapbase='snap_')
    t = s.getTime()
    #s.plot_Aslice('rho',logplot=True, cmap='BuPu', res=1024, numthreads=8)
    #s.plot_Aslice('rho', logplot=True, proj=True, axes=[0,1], numthreads=8, box=[120,120,120], vrange=[10,1e7], cmap='BuPu')
    s.plot_Aslice('rho', logplot=True, proj=False, axes=[0,1], numthreads=8, box=[10,10,10], vrange=[0.1,1000], cmap='BuPu')

    #ageStar = (t-s.data_type4['age']) * UnitTime # in Myr
    #iz = (np.abs(s.data_type4['pos'][:,2]-50.)<0.1) & (ageStar<0.01)
    #print np.sum(iz)
    #if np.sum(iz)>0:
    #    plt.scatter(s.data_type4['pos'][iz,0], s.data_type4['pos'][iz,1], marker='*', c='b', s=20, edgecolors='none')
    fig = plt.gcf()
    fig.set_size_inches(6,6)
    #plt.axis('off')
    plt.savefig('slice_'+outPref+'_'+str(i), bbox_inches='tight', transparent=True)
    plt.clf()
