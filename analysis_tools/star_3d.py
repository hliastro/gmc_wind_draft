from loadmodules import *
from analysis_tools import *
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

plt.style.use('niceplot2')

path = '/n/mvogelsfs01/hliastro/run/GMC/RUNs/Production/RHO20R/NBODY_boost2/output'
for i in range(41,100):
    print i
    s = gadget_readsnap(i, snappath=path, snapbase='snap_')

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(s.data_type4['pos'][:,0]-200.,
               s.data_type4['pos'][:,1]-200.,
               s.data_type4['pos'][:,2]-200., s=0.1, alpha=0.1, edgecolor='None')

    ax.set_xlim3d(-20, 20)
    ax.set_ylim3d(-20, 20)
    ax.set_zlim3d(-20, 20)
    plt.savefig('star3d_'+str(i)+'.png')
    plt.clf()
