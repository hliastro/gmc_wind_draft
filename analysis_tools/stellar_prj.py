from gadget import *
from gadget_subfind import *
from pysph import *
import calcGrid
import numpy.ma as ma
import sys

plt.style.use('niceplot2')

def stellar_prj(i, path, boxsize, res, cmap, numthreads):
    s = gadget_readsnap( i, snappath=path, loadonlytype=4, snapbase='snap_',hdf5=True, forcesingleprec=True )
    pos = s.data_type4['pos'].astype('float64')
    mass = s.data_type4['mass'].astype('float64')

    if len(pos)<100:
        return
    tree = makeTree( pos )
    hsml = tree.calcHsmlMulti( pos, pos, mass, 48, numthreads=numthreads )
    hsml = np.minimum( hsml, 4. * boxsize / 10. / res )
    hsml = np.maximum( hsml, 1.001 * boxsize /10. / res * 0.5 )
    rho = np.ones( size(mass) )
    res = 1024
    grid = calcGrid.calcGrid( pos, hsml, mass, rho, rho, res, res, res, boxsize, boxsize, boxsize, s.center[0], s.center[1], s.center[2], 1, 1, numthreads=numthreads )
    plt.imshow( np.log10(grid), interpolation='gaussian', cmap=cmap, vmin=0, vmax=3.5)
    plt.xlim(400,600)
    plt.ylim(400,600)
    plt.colorbar()
    plt.savefig("star_prj_"+str(i)+".png")
    plt.clf()


#path = '/n/mvogelsfs01/hliastro/run/GMC/RUNs/Production/RHO20T/NBODY_boost8/output'
path = sys.argv[1]
boxsize = 400
res = 1024
numthreads = 1
cmap = plt.get_cmap('Blues_r')
cmap.set_bad(color = 'k', alpha = 1.)

for i in range(61):
    stellar_prj(i, path, boxsize, res, cmap, numthreads)
