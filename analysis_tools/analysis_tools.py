import glob
import matplotlib.pyplot as plt
from loadmodules import *
import sys
plt.switch_backend('agg')

def center_of_mass(pos, mass):
    return np.average(pos, weights=mass, axis=0)

def MOI_tensor(pos, mass):
    CoM = center_of_mass(pos, mass)
    pos = pos - CoM
    Ixx = np.sum( mass*(pos[:,1]*pos[:,1]+pos[:,2]*pos[:,2]) )
    Iyy = np.sum( mass*(pos[:,0]*pos[:,0]+pos[:,2]*pos[:,2]) )
    Izz = np.sum( mass*(pos[:,0]*pos[:,0]+pos[:,1]*pos[:,1]) )
    Ixy = -1 * np.sum( mass*pos[:,0]*pos[:,1] )
    Iyz = -1 * np.sum( mass*pos[:,1]*pos[:,2] )
    Ixz = -1 * np.sum( mass*pos[:,0]*pos[:,2] )
    return np.array([[Ixx, Ixy, Ixz], [Ixy, Iyy, Iyz], [Ixz, Iyz, Izz]])

from scipy.interpolate import interp1d
def mass_contour(pos, mass):
    com = center_of_mass(pos, mass)
    pos = pos - com
    r = np.sqrt( np.sum( pos**2, axis=1 ) )

    #nbins = 1000
    #bins = np.linspace(0., r.max(), nbins+1)
    #hist, bins = np.histogram(r, bins, weights=mass)
    #bincenter = 0.5*(bins[:-1]+bins[1:])
    #fm = np.cumsum( hist )/float( np.sum(mass) )
    #f_m = interp1d(fm, bincenter)

    idxsort = np.argsort(r)
    msortr = mass[idxsort]
    mcum = np.cumsum(msortr)
    print mcum[-1], np.sum(mass)
    fmcum = mcum/mcum[-1]
    f_m = interp1d(np.insert(fmcum,0,0), np.insert(r[idxsort],0,0))
    return f_m(np.linspace(0.02,0.98,49))

def radius_mass_fraction(pos, mass, center=None, frac=0.5):
    if center is None:
        center = center_of_mass(pos, mass)
    else:
        assert(len(center)==3)
    pos = pos - center
    r = np.sqrt( np.sum( pos**2, axis=1 ) )

    idxsort = np.argsort(r)
    msortr = mass[idxsort]
    mcum = np.cumsum(msortr)
    #print mcum[-1], np.sum(mass)
    fmcum = mcum/mcum[-1]
    f_m = interp1d(np.insert(fmcum,0,0), np.insert(r[idxsort],0,0))
    return f_m(frac)

def mass_within_radius(pos, mass, radius, center=None):
    if center is None:
        center = center_of_mass(pos, mass)
    else:
        assert(len(center)==3)

    pos = pos - center
    r = np.sqrt( np.sum( pos**2, axis=1 ) )

    massEnclose = np.sum(mass[r<radius])
    return massEnclose

def vel_within_radius(pos, mass, vel, radius, center=None):
    if center is None:
        center = center_of_mass(pos, mass)
    else:
        assert(len(center)==3)

    pos = pos - center
    r = np.sqrt( np.sum( pos**2, axis=1 ) )

    velAve = np.average(vel[r<radius], weights=mass[r<radius], axis=0)
    return velAve

def pot_min_within_radius(pos, mass, pot, radius, center=None):
    if center is None:
        center = center_of_mass(pos, mass)
    else:
        assert(len(center)==3)

    pos = pos - center
    r = np.sqrt( np.sum( pos**2, axis=1 ) )

    mask = (r<radius)
    idxSub = np.argmin(pot[mask])
    idxParent = np.arange(len(mass))[mask][idxSub]

    return idxParent

def mass_profile(pos, mass, center=None, NBin=101, rMin=None, rMax=None, logBin=True):
    if center is None:
        center = center_of_mass(pos, mass)
    else:
        assert(len(center)==3)

    # re-center
    pos = pos - center
    r = np.sqrt( np.sum( pos**2, axis=1 ) )

    if rMin is None:
        rMin = np.min(r)
    if rMax is None:
        rMax = np.max(r)

    # radial bins
    if logBin:
        if rMin == 0:
            rMin = 1e-4
        rBins = np.logspace(np.log10(rMin), np.log10(rMax), NBin)
    else:
        rBins = np.linspace(rMin, rMax, NBin)

    # profile
    profile = np.zeros([NBin-1, 2])
    for i in range(NBin-1):
        if logBin:
            rCenter = np.sqrt(rBins[i]*rBins[i+1])
        else:
            rCenter = 0.5*(rBins[i]+rBins[i+1])

        index = (r>rBins[i]) & (r<rBins[i+1])

        if np.sum(index)==0:
            profile[i] = [rCenter, 0]
        else:
            area = 4/3.*np.pi*(rBins[i+1]**3-rBins[i]**3)
            rhoR = np.sum(mass[index])/area
            profile[i] = [rCenter, rhoR]

    return profile

def bound_star_iteration(s, hsoft, numthreads=1, multipole=1, direct=0):

    res = 128

    # read star particle information
    pos = s.data_type4['pos'].astype('float64')
    mass = s.data_type4['mass'].astype('float64')
    #try:
    #    pot = s.data_type4['pot'].astype('float64')
    #except KeyError:
    #    pot = np.repeat(-1e100, len(mass))
    vel2 = np.sum(s.data_type4['vel']**2, axis=1)

    # start iter
    nIter = 0
    print "initial # stars:", len(mass), "initial total mass:", np.sum(mass)
    print "Start removing particles"

    while True:
        boxsize = s.boxsize

        print "Hui debug", len(pos)

        # calculate new potential after particle removal
        force = calcGrid.calcgridforce( pos, pos, mass, res, res, boxx=boxsize, boxy=boxsize,
                                   centerx=s.center[0], centery=s.center[1], centerz=s.center[2],
                                   proj=True, boxz=boxsize, nz=res, numthreads=numthreads,
                                   multipole=1, hsoft=hsoft, direct=0, nparticle=1 )

        pot = force['grid'][:,3]*4.3e-3

        # remove unbound stars
        iBound = (0.5*vel2+pot)<0.0

        pos = pos[iBound]
        mass = mass[iBound]
        vel2 = vel2[iBound]
        nIter += 1

        if np.sum(iBound) == len(pos):
            print "All bound!"
            break
        elif np.sum(iBound) <= 10:
            print "Bound particle number < 10!"
            break
        else:
            print "Iteration", nIter, "# stars", np.sum(iBound), "Bound Mass", np,sum(mass[iBound])

    return np.sum(mass)

