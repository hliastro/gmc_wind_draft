import glob
import matplotlib.pyplot as plt
from loadmodules import *
import sys

plt.switch_backend('agg')

GAMMA = 5/3.
mSun = 1.998e33
mP = 1.67e-24
kb = 1.38e-16
pc = 3.086e18
yr = 3.15e7
UnitTime = pc/1e5/yr/1e6    # code time in Myr

path = sys.argv[1]

files = glob.glob(path+'/snap_*')

for f in np.sort(files):
    i = int(f[-8:-5])
    print i
    s = gadget_readsnap(i, snappath=path, snapbase='snap_')
    t = s.getTime()
    s.plot_Aslice('rho', logplot=True, proj=True, axes=[0,1], box=[20,20,20], vrange=[1, 1e6], numthreads=16, cmap='magma', res=1024)
    #plt.scatter(s.data_type4['pos'][:,0], s.data_type4['pos'][:,1], marker='*', c='b', s=20, edgecolors='none')
    ageStar = (t-s.data_type4['age'])# * UnitTime # in Myr
    iz = (ageStar<0.01)
    print np.sum(iz)
    if np.sum(iz)>0:
        plt.scatter(s.data_type4['pos'][iz,0], s.data_type4['pos'][iz,1], marker='*', c='b', s=20, edgecolors='none')

    plt.axis('off')
    plt.savefig('1e5_prj_'+str(i))
    plt.clf()
