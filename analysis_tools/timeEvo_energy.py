import glob
import matplotlib.pyplot as plt
from loadmodules import *
import sys
plt.switch_backend('agg')

GAMMA = 5/3.
mSun = 1.998e33
mP = 1.67e-24
kb = 1.38e-16
pc = 3.086e18
yr = 3.15e7
UnitTime = pc/1e5/yr/1e6    # code time in Myr

# read Kroupa wind data
kroupaWind = np.loadtxt('Kroupa_wind_FIRE.txt')
from scipy import interpolate
f_dm = interpolate.interp1d(kroupaWind[:,0], kroupaWind[:,1]) # fraction of mass loss
f_e = interpolate.interp1d(kroupaWind[:,0], kroupaWind[:,2])  # wind energy per mass in unit of erg/Msun

path = sys.argv[1]
files = glob.glob(path+'/snap_*')

dataOut = []

for f in np.sort(files):
    i = int(f[-8:-5])
    s = gadget_readsnap(i, snappath=path, snapbase='snap_')
    t = s.getTime() * UnitTime

    pos = s.data['pos']
    vel = s.data['vel']
    mass = s.data['mass']
    pres = s.data['pres']
    utherm = s.data['u']
    rho = s.data['rho']
    pot = s.data['pot']
    type = s.data['type']

    kinetic = 0.5*mass*(vel[:,0]**2 + vel[:,1]**2 + vel[:,2]**2)

    if (len(s.data_type4['mass'])>0):
        massStar = s.data_type4['mass']
        imass = s.data['gima']
        ageStar = t-s.data_type4['age'] * UnitTime # in Myr
        ageStar[ageStar<=0] = 0.0
        windEng = f_e(ageStar) * imass / 1e10 / mSun
        totWind = np.sum(windEng)
    else:
        totWind = 0.0

    totKin = np.sum(kinetic)
    totInt = np.sum(utherm*mass[type==0])
    totGrav = 0.5*np.sum(pot*mass)

    print t, totKin, totInt, totWind, -1*totGrav, totKin+totInt+totGrav-totWind
    dataOut.append([t, totGrav, totKin, totInt, totWind])

dataOut = np.array(dataOut)
np.savetxt('eng_evolve.txt', dataOut)

plt.plot(dataOut[:,0], -1*dataOut[:,1], label='Grav')
plt.plot(dataOut[:,0], dataOut[:,2], label='Kin')
plt.plot(dataOut[:,0], dataOut[:,3], label='Int')
plt.plot(dataOut[:,0], dataOut[:,4], label='Wind')
plt.plot(dataOut[:,0], dataOut[:,1]+dataOut[:,2]+dataOut[:,3], label='Tot')
plt.legend()
plt.savefig("kinetic_evolve.pdf")
