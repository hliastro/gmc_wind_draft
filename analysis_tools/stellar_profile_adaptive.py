import sys
import numpy as np

def density_profile()

for subhaloid in subhaloids:
    data = il.snapshot.loadSubhalo(basePath, NumSnap, subhaloid, ptNumStars)

    # center the galaxy
    galaxy_center = np.median(data['Coordinates'], axis=0)
    coord_centered = (data['Coordinates']-galaxy_center) * unit_length

    # project along a given axis
    # for now, just along z-axis !!!!
    x_proj = coord_centered[:,0]
    y_proj = coord_centered[:,1]
    r_proj = np.sqrt(x_proj*x_proj+y_proj*y_proj)

    ## profiles
    ## using log radius bin to calculate profile
    #N_radial = 200
    #r_bins = np.logspace(np.log10(np.min(r_proj)), np.log10(np.max(r_proj)), N_radial)
    #profile = np.zeros([N_radial-1,3])
    #for i in range(N_radial-1):
    #    r_min = r_bins[i]
    #    r_max = r_bins[i+1]
    #    r_center = np.sqrt(r_min*r_max)
    #    area = np.pi*(r_max*r_max-r_min*r_min)
    #    index = (r_proj>r_min) & (r_proj<r_max)
    #    if np.sum(index)==0:
    #        profile[i] = [r_center,0,0]
    #        continue
    #    else:
    #        sigma_m = np.sum( data['Masses'][index] )*unit_mass/area
    #        L_stars = 10**( 0.4*(Mag_sun-data['GFM_StellarPhotometrics'][index, band_number]) )
    #        SB_physical = np.sum(L_stars)/area
    #        profile[i] = [r_center, sigma_m, SB_physical]

    # alternative way of estimating profiles
    # the adaptive radius bins are used so that within each bin there are equal number of stars.
    N_star = 1000
    i_sort = np.argsort(r_proj)
    i = 0
    profile_stored = []
    while True:
        i_min = i*N_star
        i_max = (i+1)*N_star
        index = i_sort[i_min:i_max]

        r_seg = r_proj[index]
        r_min = np.min(r_seg)
        r_max = np.max(r_seg)
        r_median = np.median(r_seg)
        area = np.pi*(r_max*r_max-r_min*r_min)

        m_seg = (data['Masses'])[index] * unit_mass
        sigma_m = np.sum(m_seg)/area
        L_stars = 10**( 0.4*(Mag_sun-(data['GFM_StellarPhotometrics'])[index, band_number]) )
        SB_physical = np.sum(L_stars)/area

        profile_stored.append([r_median, sigma_m, SB_physical])

        if i_max>=len(r_proj):
            break

        i = i+1

    np.savetxt('profiles_adaptive/profile_'+str(subhaloid)+'.txt', np.array(profile_stored))
