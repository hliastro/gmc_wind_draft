import glob
import matplotlib.pyplot as plt
from loadmodules import *
import sys
plt.switch_backend('agg')
from analysis_tools import *

path = sys.argv[1]
outputName = sys.argv[2]
files = glob.glob(path+'/snap_*')

GAMMA = 5/3.
mSun = 1.998e33
mP = 1.67e-24
kb = 1.38e-16
pc = 3.086e18
yr = 3.15e7
UnitTime = pc/1e5/yr/1e6    # code time in Myr

NBin = 51

gasOut = []
starOut = []

for f in np.sort(files):
    i = int(f[-8:-5])
    print i
    s = gadget_readsnap(i, snappath=path, snapbase='snap_')
    t = s.getTime() * UnitTime

    if (len(s.data_type4['mass'])>NBin):
        center = center_of_mass(s.data_type4['pos'], s.data_type4['mass'])
    else:
        center = center_of_mass(s.data_type0['pos'], s.data_type0['mass'])

    print center

    # gas density profile
    if (len(s.data_type0['mass'])>10*NBin):
        profGas = mass_profile(s.data_type0['pos'], s.data_type0['mass'], center=center, NBin=NBin, logBin=True)
    else:
        profGas = np.zeros([NBin-1,2])
    gasOut.append(np.insert(profGas, 0, t))

    # stellar density profile
    if (len(s.data_type4['mass'])>10*NBin):
        profStar = mass_profile(s.data_type4['pos'], s.data_type4['mass'], center=center, NBin=NBin, logBin=True)
    else:
        profStar = np.zeros([NBin-1,2])
    starOut.append(np.insert(profStar, 0, t))

gasOut = np.array(gasOut)
starOut = np.array(starOut)

np.savetxt(outputName+'_gas.txt',gasOut, fmt="%g")
np.savetxt(outputName+'_star.txt',starOut, fmt="%g")
