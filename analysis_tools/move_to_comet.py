import os
import errno
import glob
import numpy as np
import fileinput
import sys

def mkdir_ignore(dirName):
    try:
        os.mkdir(dirName)
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise
        pass

dirTop = '/n/mvogelsfs01/hliastro/run/GMC/RUNs/Production/'
dirTrans = '/n/holylfs/TRANSFER/hliastro/GMC_RUNS/Production/NBODY/'

runs = ['RHO5T', 'RHO5R']
boosts = ['boost05', 'boost1', 'boost2', 'boost4', 'boost8']

for run in runs:
    # build the skeleton
    os.chdir(dirTrans)
    mkdir_ignore(run)
    os.chdir(run)
    for boost in boosts:
        mkdir_ignore(boost)
        os.chdir(boost)

        # copy relevant files to simulation dir
        dirNbody = dirTop + run + '/' + boost
        command = 'cp '+dirNbody+'/* .'
        print command
        os.system(command)

        command = 'cp -r '+dirNbody+'/output_nbody .'
        print command
        os.system(command)

        os.chdir('../')
