import simread.readsubfHDF5
import simread.readsnapHDF5 as rs
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.font_manager
from matplotlib.ticker import MaxNLocator, FixedLocator
import sys
from matplotlib.patches import Circle, Wedge, Polygon
from matplotlib.path import Path
from data import *

GAMMA=5/3.
nummin=0
nummax=18

matplotlib.rc('xtick.major', size=15)
matplotlib.rc('xtick.minor', size=7.5)
matplotlib.rc('ytick.major', size=15)
matplotlib.rc('ytick.minor', size=7.5)
fig = plt.figure(1, figsize=(10.0,10.0))
ax = fig.add_axes([0.1275,0.125,0.85,0.825])

for i, base in enumerate(bases):

	time_bin=np.zeros(nummax)
	thermal_mach_bin=np.zeros(nummax)
	magnetic_mach_bin=np.zeros(nummax)

	fname=base+"/snap_"+str(0).zfill(3)+'.hdf5'

        print base

	for num in range(nummin, nummax):

		fname=base+"/snap_"+str(num).zfill(3)
		head=rs.snapshot_header(fname)

		mass=rs.read_block(fname, "MASS", parttype=0)
		utherm=rs.read_block(fname, "U   ", parttype=0)
		rho=rs.read_block(fname, "RHO ", parttype=0)
		vel=rs.read_block(fname, "VEL ", parttype=0)
                pos=rs.read_block(fname, "POS ", parttype=0)

		kinetic = 0.5*mass*(vel[:,0]**2 + vel[:,1]**2 + vel[:,2]**2)

		total_kin = np.sum(kinetic)
                total_int = np.sum(utherm*mass)

                print head.time, 1.25e6*head.time, total_kin, total_int

	        ax.scatter(head.time, total_kin+total_int,color='k')
	        ax.scatter(head.time, 1.25e6*head.time,color='k', marker='*', s=20)
	        ax.scatter(head.time, total_kin,color='b')
	        ax.scatter(head.time, total_int,color='r')

prop = matplotlib.font_manager.FontProperties(size=20.0)
plt.legend(loc='upper left',prop=prop, ncol=1, numpoints=1, borderaxespad=2.0)

#ax.loglog()
ax.set_xlabel(r"time", fontsize=30)
ax.set_ylabel(r"$E$", fontsize=30, labelpad=-2.5)
#ax.set_xlim([0.9*rmin,1.1*rmax])
#ax.set_ylim([ymin,ymax])
ax.tick_params(axis='both', labelsize=20, pad=6.0)
plt.legend()
plt.savefig("../kinetic_evolve.pdf")
plt.clf()
