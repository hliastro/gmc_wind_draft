import glob
import matplotlib.pyplot as plt
from loadmodules import *
import sys
plt.switch_backend('agg')
plt.style.use('niceplot2')

GAMMA = 5/3.
mSun = 1.998e33
mP = 1.67e-24
kb = 1.38e-16
pc = 3.086e18
yr = 3.15e7
UnitTime = pc/1e5/yr/1e6    # code time in Myr

tff = 0.6

# read Kroupa wind data
kroupaWind = np.loadtxt('Kroupa_wind_FIRE.txt')
from scipy import interpolate
f_dm = interpolate.interp1d(kroupaWind[:,0], kroupaWind[:,1]) # fraction of mass loss
f_e = interpolate.interp1d(kroupaWind[:,0], kroupaWind[:,2])  # wind energy per mass in unit of erg/Msun

path = sys.argv[1]
files = glob.glob(path+'/snap_*')

dataOut = []

for f in np.sort(files):
    i = int(f[-8:-5])
    s = gadget_readsnap(i, snappath=path, snapbase='snap_')
    t = s.getTime() * UnitTime

    pos = s.data['pos']
    vel = s.data['vel']
    mass = s.data['mass']
    pres = s.data['pres']
    utherm = s.data['u']
    rho = s.data['rho']
    pot = s.data['pot']
    type = s.data['type']

    kinetic = 0.5*mass*(vel[:,0]**2 + vel[:,1]**2 + vel[:,2]**2)
    kineticGas = 0.5*s.data_type0['mass']*np.sum(s.data_type0['vel']**2, axis=1)
    kineticStar = 0.5*s.data_type4['mass']*np.sum(s.data_type4['vel']**2, axis=1)

    if (len(s.data_type4['mass'])>0):
        massStar = s.data_type4['mass']
        imass = s.data['gima']
        ageStar = t-s.data_type4['age'] * UnitTime # in Myr
        ageStar[ageStar<=0] = 0.0
        windEng = f_e(ageStar) * imass / 1e10 / mSun
        totWind = np.sum(windEng)
    else:
        totWind = 0.0

    totKin = np.sum(kinetic)
    totKinGas = np.sum(kineticGas)
    totKinStar = np.sum(kineticStar)
    totGrav = 0.5*np.sum(pot*mass)

    alphaGas = 2 * totKinGas / np.abs(totGrav)
    alphaStar = 2 * totKinStar / np.abs(totGrav)
    alpha = alphaGas + alphaStar

    print t, alphaGas, alphaStar
    dataOut.append([t, alphaGas, alphaStar])

dataOut = np.array(dataOut)
np.savetxt('alpha_evolve.txt', dataOut)

plt.plot(dataOut[:,0]/tff, dataOut[:,1], label=r'$\alpha_g$')
plt.plot(dataOut[:,0]/tff, dataOut[:,2], label=r'$\alpha_s$')
plt.plot(dataOut[:,0]/tff, dataOut[:,1]+dataOut[:,2], label=r'$\alpha$')
plt.xlim(0,2)
#plt.ylim(0.1, 600)
plt.yscale('log')
plt.legend()
plt.savefig("alpha_evolve.pdf")
