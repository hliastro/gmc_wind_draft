import os
import errno
import glob
import numpy as np
import fileinput
import sys

dirNbody = '/n/mvogelsfs01/hliastro/run/GMC/RUNs/Production/NBODY_template/'

runs = ['RHO5T', 'RHO5R']
boosts = ['boost05', 'boost1', 'boost2', 'boost4', 'boost8']

for run in runs:
    for boost in boosts:
        dirSim = '/n/mvogelsfs01/hliastro/run/GMC/RUNs/Production/'+run+'/'+boost

        # copy relevant files to simulation dir
        command = 'cp '+dirNbody+'* '+dirSim
        print command
        os.system(command)

        # walk into the simulation dir
        os.chdir(dirSim)

        # find the last snapshot
        snaps = glob.glob('./output/snap_*.hdf5')
        snaps = np.sort(snaps)
        iLast = int(snaps[-1][-8:-5])
        print iLast

        # copy snap to new folder
        try:
            os.mkdir('output_nbody')
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise
            pass
        command = 'cp ' + snaps[-1] + ' ./output_nbody/snap_'+snaps[-1][-8:-5]+'.hdf5'
        print command
        os.system(command)

        # run modify_snap.py
        command = 'python modify_snap.py ' + './output_nbody/ ' + str(iLast)
        print command
        os.system(command)

        # move the hydro snap
        #command = 'mv ./output_nbody/snap_' + snaps[-1][-8:-5] +'.hdf5' + ' ' + './output_nbody/snap_' + snaps[-1][-8:-5] + '.hdf5.old'
        #print command
        #os.system(command)

        command = 'mv ./output_nbody/new.hdf5 ' + './output_nbody/snap_' + snaps[-1][-8:-5] + '.hdf5'
        print command
        os.system(command)
