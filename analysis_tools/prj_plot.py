import glob
import matplotlib.pyplot as plt
from loadmodules import *
import sys

plt.switch_backend('agg')

GAMMA = 5/3.
mSun = 1.998e33
mP = 1.67e-24
kb = 1.38e-16
pc = 3.086e18
yr = 3.15e7
UnitTime = pc/1e5/yr/1e6    # code time in Myr

path = sys.argv[1]

files = glob.glob(path+'/snap_*')

for f in np.sort(files)[44:]:
    i = int(f[-8:-5])
    print i
    s = gadget_readsnap(i, snappath=path, snapbase='snap_')
    t = s.getTime()
    #s.plot_Aslice('u',logplot=False, colorbar=True, cblabel=True, numthreads=8)
    #s.plot_Aslice('rho',logplot=True,axes=[1,2], vrange=[1e-8,5e-3], numthreads=8)
    #s.plot_Aslice('rho',logplot=False,axes=[1,2], vrange=[1e-8,1.5e-5], numthreads=8)
    s.plot_Aslice('rho', logplot=True, proj=True, axes=[0,1], numthreads=8, box=[30,30,30], vrange=[1,1e7], res=1024, cmap='magma')

    #ageStar = (t-s.data_type4['age']) * UnitTime # in Myr
    #iz = (np.abs(s.data_type4['pos'][:,2]-50.)<0.1) & (ageStar<0.01)
    #print np.sum(iz)
    #if np.sum(iz)>0:
    #    plt.scatter(s.data_type4['pos'][iz,0], s.data_type4['pos'][iz,1], marker='*', c='b', s=50, edgecolors='none')

    fig = plt.gcf()
    fig.set_size_inches(10,10)
    plt.axis('off')
    plt.tight_layout()
    plt.savefig('prj_boost'+str(i), facecolor='k')
    plt.clf()
