import glob
import matplotlib.pyplot as plt
from loadmodules import *
import sys
plt.switch_backend('agg')

GAMMA = 5/3.
mSun = 1.998e33
mP = 1.67e-24
kb = 1.38e-16
pc = 3.086e18
yr = 3.15e7
UnitTime = pc/1e5/yr/1e6    # code time in Myr
tff = 1.8

path = sys.argv[1]
prefix = sys.argv[2]
#i = int(sys.argv[2])

filename = np.sort(glob.glob(path+'/snap_*.hdf5'))[-1]
i = int(filename.split('/')[-1][5:8])

s = gadget_readsnap(i, snappath=path, snapbase='snap_')

if len(s.data_type4['mass'])>0:
    massStar = s.data_type4['mass']
    imass = s.data_type4['gima']
    ageStar = s.data_type4['age'] * UnitTime # in Myr
    bins = np.linspace(0, np.max(ageStar), 100)
    hist, bins = np.histogram(ageStar, weights=imass, bins=bins)
    dt = np.mean( bins[1:]-bins[:-1] )
    print np.sum(imass), np.sum(s.data_type4['mass']), np.sum(s.data['mass']), np.sum(imass)/np.sum(s.data['mass'])
    np.savetxt('sfh_'+prefix+'.txt', np.column_stack((0.5*(bins[:-1]+bins[1:]), hist/dt/1e6)),
            header='t[Myr]\tSFR[Msun/yr]')
else:
    print "No stars!"
