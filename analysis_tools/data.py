which = 2

if (which==1):
	bases=["/n/home12/mvogelsberger/Development/NONIDEAL_MHD_TURB/nomhd/M3_128/output/",
	       "/n/home12/mvogelsberger/Development/NONIDEAL_MHD_TURB/ideal/M3_128_powell/output/",
	#       "/n/home12/mvogelsberger/Development/NONIDEAL_MHD_TURB/ideal/M3_128_ct/output/",
	#       "/n/home12/mvogelsberger/Development/NONIDEAL_MHD_TURB/nonideal/M3_128_powell_ohm_exp/output/",
	       "/n/home12/mvogelsberger/Development/NONIDEAL_MHD_TURB/nonideal/M3_128_powell_ohm_imp/output/"]


	tlabel=["none",
	        "ideal",
	#        "ideal (ct)",
	#        "non-ideal (exp)",
	        "non-ideal (imp)"]

	tcolor=["black", "green", "red", "blue"]

	tagname=["nomhd",
        	 "idealmhd",
	         "idealmhd_ct",
        	 "nonidealmhd_exp",
	         "nonidealmhd_imp"]


if (which==2):
	bases=["/n/mvogelsfs01/hliastro/run/GMC/RUNs/wind_methods/momentumConserve/output"
                #"/n/mvogelsfs01/hliastro/run/GMC/RUNs/decompose/turb_box/compressive/output"
                #"/n/mvogelsfs01/hliastro/run/MHD_TURB/turb_box_Max/TC0001/output",
                #"/n/mvogelsfs01/hliastro/run/MHD_TURB/turb_box_Max/TC001/output",
                #"/n/mvogelsfs01/hliastro/run/MHD_TURB/turb_box_Max/TC01/output",
                #"/n/mvogelsfs01/hliastro/run/MHD_TURB/turb_box_Max/TC1/output",
	       ]
        tlabel=['solenoid','compressive']#r"$f_{\rm TC}=0$",r"$f_{\rm TC}=10^{-3}$",r"$f_{\rm TC}=10^{-2}$",r"$f_{\rm TC}=0.1$",r"$f_{\rm TC}=1$"]

	tcolor=["black", "red", "blue", "green", "orange", "cyan"]

	tagname=["TC=0", "TC=1", "TC=1 iso"
        	 ]

