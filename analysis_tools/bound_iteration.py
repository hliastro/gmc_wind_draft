import numpy as np
from loadmodules import *

def bound_star_iteration(s, hsoft, numthreads=1, multipole=1, direct=0):

    res = 128

    # read star particle information
    pos = s.data_type4['pos'].astype('float64')
    mass = s.data_type4['mass'].astype('float64')
    #try:
    #    pot = s.data_type4['pot'].astype('float64')
    #except KeyError:
    #    pot = np.repeat(-1e100, len(mass))
    vel2 = np.sum(s.data_type4['vel']**2, axis=1)

    # start iter
    nIter = 0
    print "initial # stars:", len(mass), "initial total mass:", np.sum(mass)
    print "Start removing particles"

    while True:
        boxsize = s.boxsize

        print "Hui debug", len(pos)

        # calculate new potential after particle removal
        force = calcGrid.calcgridforce( pos, pos, mass, res, res, boxx=boxsize, boxy=boxsize,
                                   centerx=s.center[0], centery=s.center[1], centerz=s.center[2],
                                   proj=True, boxz=boxsize, nz=res, numthreads=numthreads,
                                   multipole=1, hsoft=hsoft, direct=0, nparticle=1 )

        pot = force['grid'][:,3]*4.3e-3

        # remove unbound stars
        iBound = (0.5*vel2+pot)<0.0

        if np.sum(iBound) == len(pos):
            print "All bound!"
            break
        elif np.sum(iBound) <= 5:
            print "Bound particle number < 10!"
            break
        else:
            print "Iteration", nIter, "# stars", np.sum(iBound), "Bound Mass", np,sum(mass[iBound])

        pos = pos[iBound]
        mass = mass[iBound]
        vel2 = vel2[iBound]

        nIter += 1

    return np.sum(mass)

path = '/n/mvogelsfs01/hliastro/run/GMC/RUNs/Production/RHO20T/NBODY_boost8/output/'

dataStore = []

for i in range(119,120):
    s = gadget_readsnap(i, snappath=path, snapbase='snap_')

    mBound = bound_star_iteration(s, hsoft=0.03, numthreads=8)

    print "total stellar mass =", np.sum(s.data_type4['mass'])
    print "bound mass = ", mBound
    print "bound fraction = ", mBound/np.sum(s.data_type4['mass'])
    #dataStore.append([i, np.sum(s.data_type4['mass']), mBound])

#dataStore = np.array(dataStore)

#np.savetxt("boundness_rho20t_boost8.txt", dataStore)
