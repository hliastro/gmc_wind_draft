import os

#dirName = "/n/mvogelsfs01/hliastro/run/GMC/RUNs/Production/"
dirName = "/n/holylfs/TRANSFER/hliastro/GMC_RUNS/Production/"
rs = ['RHO2R', 'RHO2T']
bs = ['05', '1', '2', '4', '8']

runs = []
output = []
for r in rs:
    for b in bs:
        runs.append(dirName+r+"/boost"+b+"/output")
        output.append(r+"_boost"+b)

#for i, run in enumerate(runs):
#    command = 'python output_properties.py ' + run + ' ' + output[i]
#    print command
#    os.system(command)

#for i, run in enumerate(runs):
#    command = 'python SFH.py ' + run + ' ' + output[i]
#    print command
#    os.system(command)

for i, run in enumerate(runs):
    command = 'python slice_plot.py ' + run + ' ' + output[i]
    print command
    os.system(command)




runs = [
        "/n/mvogelsfs01/hliastro/run/GMC/RUNs/Production/RHO5R/boost05/output",
        "/n/mvogelsfs01/hliastro/run/GMC/RUNs/Production/RHO5R/boost1/output",
        "/n/mvogelsfs01/hliastro/run/GMC/RUNs/Production/RHO5R/boost2/output",
        "/n/mvogelsfs01/hliastro/run/GMC/RUNs/Production/RHO5R/boost4/output",
        "/n/mvogelsfs01/hliastro/run/GMC/RUNs/Production/RHO5R/boost8/output",
        "/n/mvogelsfs01/hliastro/run/GMC/RUNs/Production/RHO5T/boost05/output",
        "/n/mvogelsfs01/hliastro/run/GMC/RUNs/Production/RHO5T/boost1/output",
        "/n/mvogelsfs01/hliastro/run/GMC/RUNs/Production/RHO5T/boost2/output",
        "/n/mvogelsfs01/hliastro/run/GMC/RUNs/Production/RHO5T/boost4/output",
        "/n/mvogelsfs01/hliastro/run/GMC/RUNs/Production/RHO5T/boost8/output"
        ]
output = [
        "RHO5R_boost05",
        "RHO5R_boost1",
        "RHO5R_boost2",
        "RHO5R_boost4",
        "RHO5R_boost8",
        "RHO5T_boost05",
        "RHO5T_boost1",
        "RHO5T_boost2",
        "RHO5T_boost4",
        "RHO5T_boost8"
        ]

