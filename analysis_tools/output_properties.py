from loadmodules import *
from analysis_tools import *
import glob
import matplotlib.pyplot as plt
import sys
plt.switch_backend('agg')

path = sys.argv[1]
outputName = sys.argv[2]
files = glob.glob(path+'/snap_*')

GAMMA = 5/3.
mSun = 1.998e33
mP = 1.67e-24
kb = 1.38e-16
pc = 3.086e18
yr = 3.15e7
UnitTime = pc/1e5/yr/1e6    # code time in Myr

mGMC = 5e4
rGMC = 15
hsoft = 0.03

dataOut = []

for f in np.sort(files):
    i = int(f[-8:-5])
    s = gadget_readsnap(i, snappath=path, snapbase='snap_')
    t = s.getTime() * UnitTime
    print "-------- t =", t, "-----------------------------"

    # add derived quantities
    s.data['temp'] = (GAMMA-1.0)*s.data['u']*1e10*mP/kb
    centerPot = s.data['pos'][np.argmin(s.data['pot'])]
    print "position at the deepest potential:", centerPot

    # gas properties
    print "Estimate mass components"
    mGasTot = np.sum( s.data_type0['mass'] )
    boundGas = s.data_type0['pot']+np.sum(s.data_type0['vel']**2,axis=1)<0
    unboundGas = s.data_type0['pot']+np.sum(s.data_type0['vel']**2,axis=1)>=0
    mBoundGas = np.sum( s.data_type0['mass'][boundGas] )
    mUnboundGas = np.sum( s.data_type0['mass'][unboundGas] )
    coldGas = (s.data_type0['temp']<100) & (s.data_type0['rho']>1e4)
    mColdGas = np.sum( s.data_type0['mass'][coldGas] )
    hotGas = (s.data_type0['temp']>1e5)
    mHotGas = np.sum( s.data_type0['mass'][hotGas] )
    # outflow is defined as unbound gas outside 1.5 initial radius
    outflow = (np.sqrt( np.sum( (s.data_type0['pos'] - centerPot)**2, axis=1 ) )>1.5*rGMC) & unboundGas
    mOutGas = np.sum( s.data_type0['mass'][outflow] )

    #print mGasTot, mBoundGas, mUnboundGas, mColdGas, mHotGas, mOutGas

    # star properties
    if (len(s.data_type4['mass'])>50):
        mStarTot = np.sum( s.data_type4['mass'] )
        mStarInitTot = np.sum( s.data_type4['gima'] )
        boundStar = s.data_type4['pot']+0.5*np.sum(s.data_type4['vel']**2, axis=1)<0.0
        #unboundStar = s.data_type4['pot']+np.sum(s.data_type4['vel']**2, axis=1)>=0.0
        #unboundStar = np.logical_not(boundStar)
        mBoundStar = np.sum( s.data_type4['mass'][boundStar] )
        #mUnboundStar = np.sum( s.data_type4['mass'][unboundStar] )
        mUnboundStar = mStarTot - mBoundStar
        #mBoundStarIter = bound_star_iteration(s, hsoft=hsoft, numthreads=8)
        mBoundStarIter = 0.0
    else:
        mStarTot = mStarInitTot = mBoundStar = mUnboundStar = mBoundStarIter = 0.0

    #print mStarTot, mStarInitTot, mBoundStar, mUnboundStar, mBoundStarIter

    # shape from moment of inertia
    #MOIGas = MOI_tensor(s.data_type0['pos'], s.data_type0['mass'])
    #rMOIGas = np.sqrt(np.linalg.eig(MOIGas)[0]/np.sum(s.data_type0['mass']))
    #if (len(s.data_type4['mass'])>10):
    #    MOIStar = MOI_tensor(s.data_type4['pos'], s.data_type4['mass'])
    #    rMOIStar = np.sqrt(np.linalg.eig(MOIStar)[0]/np.sum(s.data_type4['mass']))
    #else:
    #    rMOIStar = [0.0, 0.0, 0.0]

    # half-mass radius

    print "Estimate structural parameters"
    rHalfGas = radius_mass_fraction(s.data_type0['pos'], s.data_type0['mass'], center=centerPot)
    if (len(s.data_type4['mass'])>50):
        rHalfStar = radius_mass_fraction(s.data_type4['pos'], s.data_type4['mass'], center=centerPot)
        mGasHalf = mass_within_radius(s.data_type0['pos'], s.data_type0['mass'], radius=rHalfStar, center=centerPot)
        mStarHalf = mass_within_radius(s.data_type4['pos'], s.data_type4['mass'], radius=rHalfStar, center=centerPot)
        mGas2Half = mass_within_radius(s.data_type0['pos'], s.data_type0['mass'], radius=2*rHalfStar, center=centerPot)
        mStar2Half = mass_within_radius(s.data_type4['pos'], s.data_type4['mass'], radius=2*rHalfStar, center=centerPot)
    else:
        rHalfStar = mGasHalf = mStarHalf = mGas2Half = mStar2Half = 0.0

    # energies budget of the whole clouds/within twice-half-stellar-mass radius
    print "Estimate energy budget"
    rGas = np.sqrt( np.sum( (s.data_type0['pos'] - centerPot)**2, axis=1 ) )
    rStar = np.sqrt( np.sum( (s.data_type4['pos'] - centerPot)**2, axis=1 ) )
    idx2rGas = rGas<2.0*rHalfStar
    idx2rStar = rStar<2.0*rHalfStar
    kineticGas = np.sum( 0.5*s.data_type0['mass']*np.sum(s.data_type0['vel']**2,axis=1) )
    kineticGas2r = np.sum( 0.5*s.data_type0['mass'][idx2rGas]*np.sum(s.data_type0['vel']**2,axis=1)[idx2rGas] )
    kineticStar = np.sum( 0.5*s.data_type4['mass']*np.sum(s.data_type4['vel']**2,axis=1) )
    kineticStar2r = np.sum( 0.5*s.data_type4['mass'][idx2rStar]*np.sum(s.data_type4['vel']**2,axis=1)[idx2rStar] )
    gravGas = 0.5*np.sum( s.data_type0['pot']*s.data_type0['mass'] )
    gravGas2r = 0.5*np.sum( s.data_type0['pot'][idx2rGas]*s.data_type0['mass'][idx2rGas] )
    gravStar = 0.5*np.sum( s.data_type4['pot']*s.data_type4['mass'] )
    gravStar2r = 0.5*np.sum( s.data_type4['pot'][idx2rStar]*s.data_type4['mass'][idx2rStar] )
    #imass = s.data_type4['gima']
    #ageStar = t-s.data_type4['age'] * UnitTime # in Myr
    #ageStar[ageStar<=0] = 0.0
    #windEng = f_e(ageStar) * imass / 1e10 / mSun
    #totWind = np.sum(windEng)
    #alphaGas = -2 * kineticGas / (gravGas+gravStar)
    #alphaGas2r = -2 * kineticGas2r / (gravGas2r+gravStar)
    #alphaStar = -2 * kineticStar / (gravGas+gravStar)
    #print alphaGas, alphaStar, alphaGas2r

    dataOut.append([t, mGasTot, mBoundGas, mUnboundGas, mColdGas, mHotGas, mOutGas, mStarTot, mStarInitTot, mBoundStar, mUnboundStar, mBoundStarIter, rHalfGas, rHalfStar, mGasHalf, mGas2Half, mStarHalf, mStar2Half, kineticGas, kineticGas2r, kineticStar, kineticStar2r, gravGas, gravGas2r, gravStar, gravStar2r])

dataOut = np.array(dataOut)
np.savetxt('out_'+outputName+'.txt',dataOut,header='t, mGasTot, mBoundGas, mUnboundGas, mColdGas, mHotGas, mOutGas, mStarTot, mStarInitTot, mBoundStar, mUnboundStar, mBoundStarIter, rHalfGas, rHalfStar, mGasHalf, mGas2Half, mStarHalf, mStar2Half, kineticGas, kineticGas2r, kineticStar, kineticStar2r, gravGas, gravGas2r, gravStar, gravStar2r', fmt="%g")
