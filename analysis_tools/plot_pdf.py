import simread.readsubfHDF5
import simread.readsnapHDF5 as rs
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.font_manager
from matplotlib.ticker import MaxNLocator, FixedLocator
import sys
#import conversions as co
from matplotlib.patches import Circle, Wedge, Polygon
from matplotlib.path import Path
from data import *

plt.style.use('niceplot')

nummin=4 #250
nummax=5 #275


bmin=-7
bmax=-4
bins = np.logspace(-1.5,2.5,100)
#bins = 100

matplotlib.rc('xtick.major', size=15)
matplotlib.rc('xtick.minor', size=7.5)
matplotlib.rc('ytick.major', size=15)
matplotlib.rc('ytick.minor', size=7.5)
fig = plt.figure(1, figsize=(10.0,10.0))
ax = fig.add_axes([0.1275,0.125,0.85,0.825])


for i,base in enumerate(bases):

	first=True
	for num in range(nummin, nummax):

		fname=base+"/snap_"+str(num).zfill(3)+'.hdf5'

		print fname

		head=rs.snapshot_header(fname)

		mass=rs.read_block(fname, "MASS", parttype=0)
		rho=rs.read_block(fname, "RHO ", parttype=0)
		vol = mass/rho
                rho_mean = np.sum(mass)/np.sum(vol)
                rho = rho/rho_mean

                print np.mean(np.log10(rho))

		#h, x = np.histogram(np.log10(rho), weights=vol, range=(bmin,bmax),bins=bins)
		h, x = np.histogram(rho, weights=vol, bins=bins)


		if (first):
			htot = h
			first = False
		else:
			htot = htot + h

	x=0.5*(x[:-1]+x[1:])
	ax.plot(x, htot/htot.sum(), '-', lw=4.0, label=tlabel[i], color=tcolor[i])



prop = matplotlib.font_manager.FontProperties(size=20.0)
plt.legend(loc='upper left',prop=prop, ncol=1, numpoints=1, borderaxespad=2.0)

#ax.semilogy()
ax.set_xscale('log')
ax.set_yscale('log')
#ax.set_xlabel(r"$\log_{10}\rho$", fontsize=30)
ax.set_xlabel(r"$\rho/\bar{\rho}$", fontsize=30)

ax.set_ylabel(r"pdf", fontsize=30, labelpad=-2.5)
#ax.set_xlim([bmin,bmax])
#ax.set_ylim([1.e-4,1])
ax.tick_params(axis='both', labelsize=20, pad=6.0)
plt.legend()
plt.savefig("../pdf_rho.pdf")
plt.clf()
